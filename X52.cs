﻿/**
 *  ED X52 MFD Controller - Adds information parsed from the Elite Dangerous log files to the Saitek X52 Multi-Function Display.
 *  Copyright (C) 2015 Misha Kononov
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 **/

using System;
using System.Collections.Generic;
using DirectOutputCSharpWrapper;

namespace ED_X52_MFD_Controller
{
    class X52 : IDisposable
    {
        const uint WHEEL_PRESS = 1;
        const uint WHEEL_UP = 2;
        const uint WHEEL_DOWN = 4;

        const int DEFAULT_PAGE = 0;
        Int32 currentPage = DEFAULT_PAGE;

        DirectOutput DeviceInterface;
        IntPtr device;

        static String template = "Cmdr {commander}\n{system}\n{station}\nMode:{playmode}";
        UpdateCollection currentValues = new UpdateCollection();
        List<MfdPage> pages = new List<MfdPage>();

        //keep the event handler in an varaible to avoid garbage collection and
        //that the callback "dissapears" and causes a NullReferenceException
        private DirectOutputCSharpWrapper.DirectOutput.SoftButtonCallback buttonHandler;
        private uint prevButtons;


        public X52()
        {
            pages.Add(new MfdPage(template));
            buttonHandler = DirectOutputSoftButton;

            DeviceInterface = new DirectOutput();
            DeviceInterface.Initialize("ED X52 MFD Controller");
            DeviceInterface.Enumerate(DirectOutputDeviceEnumerate);
            DeviceInterface.RegisterDeviceCallback(DirectOutputDeviceChanged);
        }

        public void Dispose()
        {
            DeviceInterface.Deinitialize();
        }


        #region DirectOutput Handlers

        private void DirectOutputDeviceEnumerate(IntPtr dev, IntPtr target)
        {
            if (this.device != new IntPtr(0))
            {
                throw new Exception("Attempting to initialize DirectOutput device when it is alrady initialized.");
            }
#if DEBUG
            Console.WriteLine(String.Format("Adding new DirectOutput device {0} of type: {1}", dev, DeviceInterface.GetDeviceType(dev)));
#endif
            this.device = dev;
            //buttons.ButtonEvent += buttons_ButtonEvent;
            DeviceInterface.AddPage(device, DEFAULT_PAGE, DirectOutput.IsActive);
            DeviceInterface.RegisterPageCallback(this.device, DirectOutputDevicePageChanged);
            DeviceInterface.RegisterSoftButtonCallback(this.device, buttonHandler);
            RefreshDevicePage(DEFAULT_PAGE);
        }


        private void DirectOutputDeviceChanged(IntPtr dev, bool added, IntPtr target)
        {
            if (added)
            {
                DirectOutputDeviceEnumerate(dev, target);
            }
            else
            {
#if DEBUG
                Console.WriteLine(String.Format("Device {0} removed from the system.", dev));
#endif
                if (dev == this.device)
                {
                    this.device = new IntPtr(0);
                }
            }
        }

        private void DirectOutputDevicePageChanged(IntPtr dev, Int32 page, bool activated, IntPtr target)
        {
            if (activated)
            {
                PageActivated(page);
            }
            else
            {
                PageDeactivated(page);
            }
        }

        private void DirectOutputSoftButton(IntPtr device, uint buttons, IntPtr target)
        {
            if (buttons != prevButtons)
            {
                uint button = buttons ^ prevButtons;
                bool state = (buttons & button) == button;
                prevButtons = buttons;

                OnButton(button, state);
            }
        }

        #endregion

        /// <summary>
        /// Use this to update the values available to templates
        /// </summary>
        /// <param name="updates"></param>
        public void UpdateValues(UpdateCollection updates)
        {
            string t = template;
            List<String> changes = new List<String>();
            foreach (string key in updates.Keys)
            {
                if (UpdateValue(key, updates[key]))
                {
                    changes.Add(key);
                }
            }

            if (changes.Count > 0)
            {
                RefreshDevicePage(currentPage);
            }
        }

        private void PageActivated(Int32 page)
        {
            currentPage = page;
            RefreshDevicePage(page);
        }

        private void PageDeactivated(int page)
        {
            //TODO: perhaps something
        }

        /// <summary>
        /// What should happen on button press
        /// </summary>
        /// <param name="button">number of the button</param>
        /// <param name="state">true on press, false on release</param>
        private void OnButton(uint button, bool state)
        {
            if (state)
            {
                //flag indicating if current page should be changed or not.
                bool pageChanged = false; 
                switch (button)
                {
                    case WHEEL_UP:
                        pageChanged = pages[currentPage].LineUp();
                        break;
                    case WHEEL_DOWN:
                        pageChanged = pages[currentPage].LineDown();
                        break;
                    case WHEEL_PRESS:
                        //TODO:What should happen on press?
                        break;
                }

                if (pageChanged)
                {
                    RefreshDevicePage(currentPage);
                }
            }

        }

        /// <summary>
        /// Updates a line of the MFD display page
        /// </summary>
        /// <param name="page">Page number</param>
        /// <param name="line">Line number</param>
        /// <param name="msg">Message</param>
        /// <param name="args">Applies params using String.Format</param>
        private void WriteMFDLine(int page, int line, string msg, params object[] args)
        {
            if (device != new IntPtr(0) && !String.IsNullOrEmpty(msg))
            {
                string text;
                if (args.Length > 0)
                {
                    text = String.Format(msg, args);
                }
                else
                {
                    text = msg;
                }
                DeviceInterface.SetString(device, page, line, text);
            }
        }

        /// <summary>
        /// Update one of the keys with a new value if different from current
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="value">the new value</param>
        /// <returns>True if value is changed</returns>
        private bool UpdateValue(string key, string value)
        {
            if (!currentValues.ContainsKey(key))
            {
                currentValues.Add(key, value);
                return true;
            }
            {
                if (currentValues[key] != value)
                {
                    currentValues[key] = value;
                    return true;
                }
            }
            return false;
        }


        public void RefreshDevicePage(Int32 page)
        {
            if (pages.Count >= page)
            {
                var newlines = pages[page].Redraw(currentValues);

                for (int i = 0; i < newlines.Length; i++)
                {
                    WriteMFDLine(DEFAULT_PAGE, i, newlines[i]);
                }
            }
        }
    }
}
