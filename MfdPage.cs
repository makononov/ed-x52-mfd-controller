﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ED_X52_MFD_Controller
{
    public class MfdPage
    {
        private string template;
        public List<string> lines;

        private int currentLine = 0;
        private string[] availablePage;


        public MfdPage(string template)
        {
            this.template = template;
            availablePage = RenderPage(null);
        }

        public bool LineUp()
        {
            if (currentLine > 0)
            {
                currentLine--;
                return true;
            }
            return false;
        }

        public bool LineDown()
        {
            if (currentLine < availablePage.Length - 1)
            {
                currentLine++;
                return true;
            }
            return false;
        }

        public string[] Redraw(UpdateCollection values)
        {
            availablePage = RenderPage(values);
            string[] visible = new string[3];
            var length = availablePage.Count();
            if(length > 3) {
                length = 3;
            }

            if (currentLine > availablePage.Count() - 3)
            {
                currentLine = availablePage.Count() - 3;
                if (currentLine < 0)
                {
                    currentLine = 0;
                }
            }

            Array.Copy(availablePage, currentLine, visible, 0, length);
            return visible;
        }

        private string[] RenderPage(UpdateCollection values)
        {
            string t = template;
            if (values != null) 
            { 
                foreach (string key in values.Keys)
                {
                    t = t.Replace("{" + key + "}", values[key]);
                }
            }
            var available = t.Split('\n');
            return available;
        }

        
    }
}
